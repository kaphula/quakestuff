# Nicer Quake 1 expansion pack 2 (Rogue) support for Trenchbroom

Some additional Rogue engine definitions might be missing since the main
purpose was to add support for new monsters and weapons. If you want to include
all of the Rogue functionality, check out the "add_more_rogue_from_here.fgd"
and copy paste the new required entity definitions to "Rogue.fgd".
"rogue_model_progs_list.txt can be used to match the entity's model attribute
for better editing experience with Trenchbroom.

## How to install:

1) Add "Quake Rogue" folder to your local Trenchbroom folder, for example on Linux:

/home/myuser/.Trenchbroom/Quake Rogue

Paste everything there.

2) Open Trenchbroom and create new map and select the game as "Quake Rogue"

3) Make sure you choose the correct entity definitions from:

Entity -> Entity Definitions -> Rogue.fgd


## TODO:

1) Add correct working models based on spawnflags for Ogre, Spawn and Mummy
